﻿using System.Net;

namespace JTTT
{
    public class TasksList
    {
        public void AddTask(int con, int act, string url, string word, string town, int temperature, string email)
        {
            using (var ctx = new TaskDbContext())
            {
                var task = new Task() { Con = con, Act = act, Condition = new Condition() { Url = url, Word = word, Town = town, Temperature = temperature}, Action = new Action() { Email = email } };
                ctx.Task.Add(task);
                ctx.SaveChanges();
            }
        }

        public void RemoveAll()
        {
            using (var ctx = new TaskDbContext())
            {
                foreach (var t in ctx.Task)
                {
                    ctx.Task.Remove(t);
                }
                ctx.SaveChanges();
            }
            using (var ctx = new TaskDbContext())
            {
                foreach (var c in ctx.Condition)
                {
                    ctx.Condition.Remove(c);
                }
                ctx.SaveChanges();
            }
            using (var ctx = new TaskDbContext())
            {
                foreach (var a in ctx.Action)
                {
                    ctx.Action.Remove(a);
                }
                ctx.SaveChanges();
            }
        }

        public string Do()
        {
            using (var ctx = new TaskDbContext())
            {
                foreach (var t in ctx.Task)
                {
                    int a = t.ActionId;
                    int c = t.ConditionId;
                    Action act = ctx.Action.Find(a);
                    Condition con = ctx.Condition.Find(c);

                    if (t.Con == 0 && t.Act == 1)
                    {
                        GetImage(con.Url, con.Word);

                        var test = new InfoWindow(con.Url, con.Word);
                        test.ShowDialog();

                        MakeLog(con.Url, con.Word, "");
                    }
                    else if (t.Con == 1 && t.Act == 1)
                    {
                        var weather = new CheckWeather(con.Town);
                        var root = weather.ConvertJson();
                        double temp = root.main.temp - 273.15;
                        string url = "http://openweathermap.org/img/w/" + root.weather[0].icon + ".png";

                        using (var webClient = new WebClient())
                        {
                            webClient.DownloadFile(url, con.Town + ".jpg");
                        }

                        var test = new InfoWindow(url, temp.ToString(), con.Town);
                        test.ShowDialog();

                        MakeLog(url, temp.ToString() + ", " + con.Town, "");

                        if (temp > con.Temperature)
                        {
                            return "Jest cieplej!";
                        }
                        else
                        {
                            return "Jest zimniej!";
                        }
                    }
                    else if (t.Act == 0 && t.Con == 0)
                    {
                        GetImage(con.Url, con.Word);

                        CreateMail(con.Url, con.Word, act.Email);

                        MakeLog(con.Url, con.Word, act.Email);
                    }
                    else if (t.Con == 1 && t.Act == 0)
                    {
                        var weather = new CheckWeather(con.Town);
                        var root = weather.ConvertJson();
                        double temp = root.main.temp - 273.15;
                        string url = "http://openweathermap.org/img/w/" + root.weather[0].icon + ".png";

                        using (var webClient = new WebClient())
                        {
                            webClient.DownloadFile(url, con.Town + ".jpg");
                        }

                        CreateMail(url, con.Town, act.Email);
                        
                        MakeLog(url, temp.ToString() + ", " + con.Town, act.Email);

                        if (temp > con.Temperature)
                        {
                            return "Jest cieplej!";
                        }
                        else
                        {
                            return "Jest zimniej!";
                        }
                    }
                }
            }
            return "";
        }

        private void GetImage(string url, string word)
        {
            var site = new HtmlPage(url, word);
            site.FindImage();
        }

        private void CreateMail(string url, string word, string email)
        {
            var mail = new Mail(email);
            mail.SendMail(url, word);
        }

        private void MakeLog(string url, string word, string email)
        {
            var log = new Log();
            log.SaveLog(url, word, email);
        }
    }
}
