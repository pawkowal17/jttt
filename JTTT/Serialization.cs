﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace JTTT
{
    public class Serialization
    {
        public void Serialize()
        {
            var list = new List<Task>();
            var write = new FileStream("serialize.dat", FileMode.Create);
            var formatter = new BinaryFormatter();

            using (var ctx = new TaskDbContext())
            {
                foreach (var t in ctx.Task)
                {
                    list.Add(t);
                }
            }

            try
            {
                formatter.Serialize(write, list);
            }
            catch (SerializationException e)
            {
                Console.WriteLine(@"Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                write.Close();
            }
        }

        public void Deserialize()
        {
            var list = new List<Task>();
            var read = new FileStream("serialize.dat", FileMode.Open);
            var formatter = new BinaryFormatter();

            try
            {
                list = (List<Task>)formatter.Deserialize(read);
            }
            catch (SerializationException e)
            {
                Console.WriteLine(@"Failed to deserialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                read.Close();
            }

            foreach (Task task in list)
            {
                using (var ctx = new TaskDbContext())
                {
                    ctx.Task.Add(task);
                    ctx.SaveChanges();
                }
            }
        }
    }
}
