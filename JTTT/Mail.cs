﻿using System;
using System.Net;
using System.Net.Mail;

namespace JTTT
{
    class Mail
    {
        private readonly string _email;

        public Mail(string email)
        {
            _email = email;
        }

        public void SendMail(string url, string word)
        {
            using (var client = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "JTTT.test123@gmail.com",
                    Password = "JTTTtest123"
                };
                client.Credentials = credential;
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;

                var message = new MailMessage();

                try
                {
                    message.To.Add(new MailAddress(_email));
                    message.From = new MailAddress("JTTT.test123@gmail.com");
                    message.Subject = "Image for: " + word;
                    message.Body = url;
                    message.IsBodyHtml = true;
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e);
                }

                try
                {
                    using (var attachment = new Attachment(word + ".jpg", System.Net.Mime.MediaTypeNames.Image.Jpeg))
                    {
                        message.Attachments.Add(attachment);
                        client.Send(message);
                    }
                }
                catch (System.IO.FileNotFoundException e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
