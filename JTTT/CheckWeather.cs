﻿using System.Net;
using System.Text;
using JTTT.Models;
using Newtonsoft.Json;

namespace JTTT
{
    public class CheckWeather
    {
        private readonly string _town;

        public CheckWeather(string town)
        {
            _town = town;
        }

        public string GetJson()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var str = "http://api.openweathermap.org/data/2.5/weather?q=" + _town + ",pl&APPID=1492a717dfe22b8351ade27c0b03a53b";
                var json = wc.DownloadString(str);

                return json;
            }
        }

        public RootObject ConvertJson()
        {
            string data = GetJson();
            var root = JsonConvert.DeserializeObject<RootObject>(data);
            return root;
        }
    }
}
