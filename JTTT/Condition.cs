﻿using System;

namespace JTTT
{
    [Serializable]
    public class Condition
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Word { get; set; }
        public string Town { get; set; }
        public int? Temperature { get; set; }
        
        public override string ToString()
        {
            return string.Format(", Condition Id = {0}, Url = {1}, Word = {2}, Town = {3}", Id, Url, Word, Town);
        }
    }
}
