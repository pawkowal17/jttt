﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace JTTT
{
    public partial class InfoWindow : Form
    {
        private readonly string _url;
        private readonly string _word;
        private readonly string _temp;
        private readonly string _town;

        public InfoWindow(string url, string word)
        {
            _url = url;
            _word = word;

            InitializeComponent();

            infoBox.Text = "Ze strony: " + _url + " Obrazek ze słowem: " + _word;
            string img = _word + ".jpg";
            imageBox.SizeMode = PictureBoxSizeMode.StretchImage;
            imageBox.Image = Image.FromFile(img);
        }

        public InfoWindow(string url, string temp, string town)
        {
            _url = url;
            _temp = temp;
            _town = town;

            InitializeComponent();

            infoBox.Text = "Temperatura w mieście " + _town + " wynosi " + _temp;
            string img = _town + ".jpg";
            imageBox.Image = Image.FromFile(img);
        }

        private void InfoWindow_Load(object sender, EventArgs e)
        {

        }
    }
}
