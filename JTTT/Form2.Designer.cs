﻿namespace JTTT
{
    partial class InfoWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.infoBox = new System.Windows.Forms.RichTextBox();
            this.imageBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // infoBox
            // 
            this.infoBox.Location = new System.Drawing.Point(108, 26);
            this.infoBox.Name = "infoBox";
            this.infoBox.Size = new System.Drawing.Size(415, 199);
            this.infoBox.TabIndex = 0;
            this.infoBox.Text = "";
            // 
            // imageBox
            // 
            this.imageBox.Location = new System.Drawing.Point(586, 26);
            this.imageBox.Name = "imageBox";
            this.imageBox.Size = new System.Drawing.Size(841, 472);
            this.imageBox.TabIndex = 1;
            this.imageBox.TabStop = false;
            // 
            // InfoWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1439, 510);
            this.Controls.Add(this.imageBox);
            this.Controls.Add(this.infoBox);
            this.Name = "InfoWindow";
            this.Text = "InfoWindow";
            this.Load += new System.EventHandler(this.InfoWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox infoBox;
        private System.Windows.Forms.PictureBox imageBox;
    }
}