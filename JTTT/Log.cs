﻿using System;
using System.IO;

namespace JTTT
{
    class Log
    {
        public void SaveLog(string url, string word, string email)
        {
            using (var logFile = File.AppendText("jttt.log"))
            {
                logFile.Write("\r\nLog Entry : ");
                logFile.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                logFile.WriteLine("  :");
                logFile.WriteLine("  :{0}", url);
                logFile.WriteLine("  :{0}", word);
                logFile.WriteLine("  :{0}", email);
                logFile.WriteLine("-------------------------------");
            }
        }
    }
}
