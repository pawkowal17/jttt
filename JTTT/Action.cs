﻿using System;

namespace JTTT
{
    [Serializable]
    public class Action
    {
        public int Id { get; set; }
        public string Email { get; set; }
        
        public override string ToString()
        {
            return string.Format(", Action  Id = {0}, Email = {1}", Id, Email);
        }
    }
}
