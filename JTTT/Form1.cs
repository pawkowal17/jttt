﻿using System;
using System.Windows.Forms;

namespace JTTT
{
    public partial class TaskWindow : Form
    {
        public TaskWindow()
        {
            InitializeComponent();
        }

        private void TaskWindow_Load(object sender, EventArgs e)
        {
            urlBox.Text = @"http://demotywatory.pl";
            wordBox.Text = @"szukana fraza";
            emailBox.Text = @"twój email";
            townBox.Text = @"Wroclaw";
            tempBox.Text = @"15";
            infoLabel.Text = @"";
            UpdateListBox();
        }

        private void AddButton(object sender, EventArgs e)
        {
            var tl = new TasksList();
            int temp = Int32.Parse(tempBox.Text);
            tl.AddTask(tabControl1.SelectedIndex, tabControl2.SelectedIndex, urlBox.Text, wordBox.Text, townBox.Text, temp, emailBox.Text);
            UpdateListBox();
        }

        private void DoButton(object sender, EventArgs e)
        {
            var tl = new TasksList();
            infoLabel.Text = tl.Do();
        }

        private void SerializeButton(object sender, EventArgs e)
        {
            var sl = new Serialization();
            sl.Serialize();
        }

        private void DeserializeButton(object sender, EventArgs e)
        {
            var sl = new Serialization();
            sl.Deserialize();
            UpdateListBox();
        }

        private void RemoveButton(object sender, EventArgs e)
        {
            var tl = new TasksList();
            tl.RemoveAll();
            UpdateListBox();
        }

        private void UpdateListBox()
        {
            listBox1.Items.Clear();
            using (var ctx = new TaskDbContext())
            {
                foreach (var t in ctx.Task)
                {
                    int a = t.ActionId;
                    int c = t.ConditionId;
                    Action act = ctx.Action.Find(a);
                    Condition con = ctx.Condition.Find(c);

                    string str = t.ToString() + act.ToString() + con.ToString();

                    listBox1.Items.Add(str);
                }
            }
        }
    }
}
