namespace JTTT.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Conditions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Url = c.String(),
                        Word = c.String(),
                        Town = c.String(),
                        Temperature = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Con = c.Int(nullable: false),
                        Act = c.Int(nullable: false),
                        ConditionId = c.Int(nullable: false),
                        ActionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Actions", t => t.ActionId, cascadeDelete: true)
                .ForeignKey("dbo.Conditions", t => t.ConditionId, cascadeDelete: true)
                .Index(t => t.ConditionId)
                .Index(t => t.ActionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "ConditionId", "dbo.Conditions");
            DropForeignKey("dbo.Tasks", "ActionId", "dbo.Actions");
            DropIndex("dbo.Tasks", new[] { "ActionId" });
            DropIndex("dbo.Tasks", new[] { "ConditionId" });
            DropTable("dbo.Tasks");
            DropTable("dbo.Conditions");
            DropTable("dbo.Actions");
        }
    }
}
