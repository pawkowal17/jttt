﻿using System.Data.Entity;

namespace JTTT
{
    public class TaskDbContext : DbContext
    {
        public TaskDbContext()
            :base("TaskDatabase")
        {
            Database.SetInitializer(new TaskDbInitializer());
        }

        public DbSet<Condition> Condition { get; set; }
        public DbSet<Action> Action { get; set; }
        public DbSet<Task> Task { get; set; }
    }
}
