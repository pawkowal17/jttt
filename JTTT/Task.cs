﻿using System;

namespace JTTT
{
    [Serializable]
    public class Task
    {
        public int Id { get; set; }
        public int Con { get; set; }
        public int Act { get; set; }

        public int ConditionId { get; set; }
        public virtual Condition Condition { get; set; } = new Condition();

        public int ActionId { get; set; }
        public virtual Action Action { get; set; } = new Action();

        public override string ToString()
        {
            return string.Format("Task Id = {0}", Id);
        }
    }
}
