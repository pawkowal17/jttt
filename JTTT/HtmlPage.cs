﻿using System;
using System.Net;
using System.Text;
using HtmlAgilityPack;

namespace JTTT
{
    class HtmlPage
    {
        private readonly string _url;
        private readonly string _word;

        public HtmlPage(string url, string word)
        {
            _url = url;
            _word = word;
        }

        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = WebUtility.HtmlDecode(wc.DownloadString(_url));

                return html;
            }
        }

        public void FindImage()
        {
            var doc = new HtmlDocument();

            try
            {
                var pageHtml = GetPageHtml();
                doc.LoadHtml(pageHtml);
            }
            catch (WebException e)
            {
                Console.WriteLine(e);
            }

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("alt", "").Contains(_word))
                {
                    using (var webClient = new WebClient())
                    {
                        webClient.DownloadFile(node.GetAttributeValue("src", ""), _word + ".jpg");
                    }
                    return;
                }
            }
        }
    }
}
