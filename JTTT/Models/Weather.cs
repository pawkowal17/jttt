﻿namespace JTTT.Models
{
    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }

        public override string ToString()
        {
            return string.Format("id = {0}, main = {1}, description = {2}, icon = {3}", id, main, description, icon);
        }
    }
}
