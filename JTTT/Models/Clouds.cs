﻿namespace JTTT.Models
{
    public class Clouds
    {
        public int all { get; set; }

        public override string ToString()
        {
            return string.Format("all = {0}", all);
        }
    }
}
