﻿namespace JTTT.Models
{
    public class Main
    {
        public double temp { get; set; }
        public int pressure { get; set; }
        public int humidity { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }

        public override string ToString()
        {
            return string.Format("temp = {0}, pressure = {1}, humidity = {2}, temp_min = {3}, temp_max = {4}", temp, pressure, humidity, temp_min, temp_max);
        }
    }
}
