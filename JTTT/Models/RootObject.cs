﻿using System;
using System.Collections.Generic;

namespace JTTT.Models
{
    public class RootObject
    {
        public Coord coord { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public int visibility { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public Sys sys { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }

        private string result;

        public override string ToString()
        {

            foreach (var we in weather)
            {
                result = String.Join(", ", we.ToString());
            }

            return string.Format("coord = {0}, weather = {1}, @base = {2}, main = {3}, visibility = {4}, wind = {5}, clouds = {6}, dt = {7}, sys = {8}, id = {9}, name = {10}, cod = {11}", coord, result, @base, main, visibility, wind, clouds, dt, sys, id, name, cod);
        }
    }
}
