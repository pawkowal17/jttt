﻿namespace JTTT.Models
{
    public class Wind
    {
        public double speed { get; set; }
        public int deg { get; set; }

        public override string ToString()
        {
            return string.Format("speed = {0}, deg = {1}", speed, deg);
        }
    }
}
