﻿namespace JTTT.Models
{
    public class Sys
    {
        public int type { get; set; }
        public int id { get; set; }
        public double message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }

        public override string ToString()
        {
            return string.Format("type = {0}, id = {1}, message = {2}, country = {3}, sunrise = {4}, sunset = {5}", type, id, message, country, sunrise, sunset);
        }
    }
}
