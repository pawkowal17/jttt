﻿namespace JTTT.Models
{
    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }

        public override string ToString()
        {
            return string.Format("lat = {0}, lon = {1}", lon, lat);
        }
    }
}
